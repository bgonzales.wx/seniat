const routes = [
  {
    path: '/menu',
    component: () => import('layouts/Menu.vue'),
    children: [
      { path: '', component: () => import('pages/Index.vue') },
      { path: '/user', component: () => import('pages/Users/List.vue') },
      { path: '/user/agg_user', component: () => import('pages/Users/Form.vue') },
      { path: '/user/edit_user/:id', component: () => import('pages/Users/Form.vue') },
      { path: '/plants', component: () => import('pages/Plants/List.vue') },
      { path: '/plants/agg_plants', component: () => import('pages/Plants/Form.vue') },
      { path: '/plants/edit_plants/:id', component: () => import('pages/Plants/Form.vue') }
    ]
  },
  {
    path: '/qrcodefito/consulta/index/:number/:code',
    component: () => import('layouts/ConsultList.vue')
  },
  {
    path: '/qrcodefito',
    component: () => import('layouts/Consult.vue')
  },
  {
    path: '/qrcodefito/consulta/validar',
    component: () => import('layouts/Validar.vue')
  },
  {
    path: '/redict',
    component: () => import('layouts/VerifyConection.vue')
  },
  {
    path: '/login',
    component: () => import('layouts/Login.vue')
  },
  {
    path: '*',
    redirect: '/login'
  }
]

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/Error404.vue')
  })
}
export default routes
